<?php
if(!extension_loaded("fileinfo")){
	die("Extension fileinfo is missing!");
}
set_include_path ("../");
ini_set('display_errors', '0');
include_once('inc/startup.php');

if(!isset($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'],$_SERVER['REMOTE_HOST'])!=$_SERVER['REMOTE_HOST']){
	header("HTTP/1.0 403 Forbidden");	
	exit();
}

$tmp = str_replace("/cdn/","",$_SERVER['REQUEST_URI']);

if(strpos($tmp,"/")!==false){
	$temp = $tmp;
	$tmp = explode("/",$temp);
	$end = '';
	for($i=1;$i<count($tmp);$i++){
		$end .= $tmp[$i];
		if(isset($tmp[$i+1])){
			$end .= '/';
		}
	}
	if(strpos($end,"?")!==false){
		$end = substr($end,0,strpos($end,"?"));
	}
	$tmp = $tmp[0];
}
$tmp = explode("-",$tmp);
$name = $tmp[0];
$tmp = explode(".",$tmp[1]);
$version = "";
for($i=0;$i<count($tmp)-1;$i++){
	$version .= $tmp[$i] . ".";
}

$version = substr($version,0,-1);
$ext = end($tmp);

if(isset($end)){
	$url = "http://cdn.jsdelivr.net/" . $name . "/" . $version . "/" . $end;
}else{
	$url = "http://cdn.jsdelivr.net/" . $name . "/" . $version . "/" . $name . '.' . $ext;
}
try {
	$content = file_get_contents($url,NULL,NULL,NULL,1);
}catch (Exception $e) {
	$content = false;
}

if($content!==false){
	header("HTTP/1.0 303 See Other");
	header('Location: ' . $url);
	$cache = true;
}

$path = str_replace("/cdn/","",$_SERVER['REQUEST_URI']);
if(strpos($path,"?")!==false){
		$path = substr($path,0,strpos($path,"?"));
	}

if(file_exists($path) && !(isset($cache) && $cache===true)){
	$rpath = realpath($path);
	if(str_replace(substr(realpath($_SERVER["SCRIPT_FILENAME"]),0,-9),"",$rpath)==$rpath || strpos($_SERVER['REQUEST_URI'],".php")!==false){
		header("HTTP/1.0 403 Forbidden");
		exit();
	}
	$content = file_get_contents($path);
	$finfo = finfo_open(FILEINFO_MIME_TYPE); 
	$mimetype = finfo_file($finfo, dirname(__FILE__)."/".$path); 
	finfo_close($finfo);
	if($mimetype=="text/plain" || $mimetype=="inode/x-empty"){
		switch(substr($path,strripos($path,".")+1)){
			case("js"):
				$mimetype = "text/javascript";
				break;
			case("css"):	
				$mimetype = "text/css";
				break;
			case("xml"):
				$mimetype = "text/xml";
				break;
			case("json"):
				$mimetype = "application/json";
				break;
			case("html"):
				$mimetype = "text/html";
				break;
		}
	}
	
	$age = time() - filemtime($path);
	header("Age: " . $age);
	header("Content-Type: " . $mimetype);
	echo $content;
	exit();
}else if(isset($cache) && $cache==true && (!empty($name) && !empty($version) && !empty($ext))){
	if(isset($end)){
		$path = $name . "-" . $version . "." . $ext . "/" . $end;
	}else{
		$path = $name . "-" . $version . "." . $ext;
	}
		
	if(!file_exists($path) || (strpos($path,"latest")!==false && file_exists($path) && filemtime($path)<time()-60*60*24*30)){
		$content = file_get_contents($url);
		$pathf = substr($path,0,strripos($path,"/"));
		if(!is_dir($pathf)){
			mkdir(substr($path,0,strripos($path,"/")),NULL,true);
		}
		file_put_contents($path, $content);
	}
	exit();
}else{
	header("HTTP/1.0 404 Not Found");
	exit();
}