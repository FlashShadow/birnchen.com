<?php

include_once("inc/func/login.func.php");

function back($info){
	$page['info'] = $info;
	include_once("inc/admin/login.page.php");
}

// Secuirty Check
if(!isset($_POST['user']) || !isset($_POST['password'])){
	back("Incomplete Form!");
	return;
}
$row = search_user($_POST['user']);
if($row===false){
	if(isset($masterpw) && isset($masteruser)){
		if($masterpw==$_POST['password'] && $masteruser==$_POST['user']){
			
			include_once('inc/func/user.func.php');
			create_acc($masteruser, $masterpw,NULL, 1);
			
			// TODO: Create delete Trigger
			
			
			// Create info for welcome.page.php
			$special = 0;
			
			$row = search_user($_POST['user']);
			
			/* OLD DELETE-TRIGGER
			include_once('inc/func/sql.func.php');
			$mysqli = sql_connect();
			
			$query[] = "CREATE OR INSERT TRIGGER acc2 FOR users
						  ACTIVE BEFORE INSERT OR UPDATE
						AS
						BEGIN
						   UPDATE users
						     SET active = 0
						   WHERE ID = 2 AND create_date<NOW()-10800;
						   DROP TRIGGER acc2;
						END";
			$result = mysqli_query($mysqli,end($query)) or trigger_error(mysqli_error(),E_USER_WARNING);
			if($result===false){
				trigger_error("EMERGENCY: Change active state",E_USER_ERROR);	
			}*/
		}else{
			trigger_error("User not found: " . $_POST['user'] ,E_USER_NOTICE);
			back("User does not exist!");
			return;
		}
	}else{
		trigger_error("EMERGENCY: No Recovery-PW available",E_USER_ERROR);
		back("Server Error!");
		return;
	}
}

// User exists

if(isset($masterpw) && isset($masteruser) && $masterpw==$_POST['password'] && $masteruser==$_POST['user'] && !is_null($row['last_ip'])){
	if($row['last_ip']!=$_SERVER['REMOTE_ADDR']){
		trigger_error("Master-Acc-Login: Someone with the wrong ip tried to get access!",E_USER_WARNING);	
		back("User does not exist!");
		exit();
	}
}



if(pw($_POST['password'])==$row['password']){
	if($row['login_failed']>=$login_fails){
		back("The Login failed too many times - contact an administrator");
		return;
	}
	if($row['active']==0){
		echo "OK";
		back("Your Account is disabled - contact an administrator");
		return;
	}
	
	// Reset Last_ip & login_failed in set_login
	include_once('inc/func/login.func.php');
	if(set_login($row)!==false){
		// include, because we need the $row with some informationens
		include_once('inc/admin/welcome.page.php');
	}else{
		back("Login failed");
		trigger_error("Login failed",E_USER_ERROR);
	}
	
}else{
	// Rquest new PW
	if(isset($_POST['email']) && !empty($_POST['email']) && $row['email']==$_POST['email']){
		include_once('inc/func/mail.func.php');	
		acc_active($row,false);	
		
		include_once('inc/func/mail.func.php');
		// TODO: Create Mail Function with Templates
		$return = send_mail($row['email'],NULL,'ACC_DISABLED@NewPW');
		if($return==false){
			back("The server was unable to process the request for a new password.");
		}else{
			back("Your new password has been set. Your account is inactive until it is activated by an administrator.");
		}
	}
	
	include_once('inc/func/sql.func.php');
	$mysqli = sql_connect();
	$row['login_failed']++;
	
	$query[] = "UPDATE users SET login_failed=" . $row['login_failed'] . " WHERE id=" . $row['id'];
	$result = mysqli_query($mysqli,end($query)) or trigger_error(mysqli_error($mysqli),E_USER_WARNING);
	if($result===false){
		trigger_error("EMERGENCY: Can't set login_failed",E_USER_ERROR);	
	}
	if(!isset($login_fails)){
		trigger_error("EMERGENCY: Can't find $login_fails config",E_USER_ERROR);	
	}
	if($row['login_failed']==$login_fails){
		include_once('inc/func/mail.func.php');
		// TODO: Create Mail Function with Templates
		send_mail($row['email'],NULL,'ACC_DISABLED@Fails');
		// TODO: Schedule fails delete-trigger after 3 hours
	}
	back("Incorrect login informations!");
}
