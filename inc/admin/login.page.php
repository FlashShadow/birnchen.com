<?php
include_once('inc/func/login.func.php');
if(check_login()==true){
	$special = 1;
	$row = $user;
	include_once('inc/admin/welcome.page.php');
	return;
}
?>
<html>
	<head>
		<title>Login Panel</title>
		<meta name="robots" content="noindex">
		<link href="/cdn/normalize-latest.css/normalize.min.css" rel="stylesheet" charset="UTF-8" />
		<style>
			body{
				background-color: #3B5998;
				z-index: 2;
				font-family: 'Times New Roman';
			}
			form{
				position: absolute;
				left: 50%;
				top: 50%;
				width: 350px;
				margin-top: -100px;
				margin-left: -175px;
				background-color: #FFFFFF;
				padding: 20px 10px;
				z-index: 3;
				outline-color: #000000;
				outline-width: 1px;
				outline-style: solid;
			}
			label{
				clear: both;
				float: left;
			}
			input{
				white-space: normal;
				float: right;
				width: 120px;
				margin-bottom: 5px;
			}
			details{
				margin-top: 20px;
			}
			img{
				margin-top: 10px;
				clear: both;
				height: 60px;
				width: 280px;
				visibility: collapse;
			}
		</style>
	</head>
	
	<form action="login.php" method="post">
		<label>Benutzername: </label>
		<input type="text" name="user" required="required" placeholder="Benutzername">
		<br>
		<label>Passwort:</label>
		<input type="password" name="password" required="required" placeholder="Passwort">
		<br>
		<details>
			<summary>Passwort vergesssen?</summary>
			<p>Fordere ein neues Passwort vom Administrator an:</p>
			<label>E-Mail: </label>
			<input type="email" name="email" placeholder="email@example.com" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\b" title="mail@example.com">
			<br>
			<p>Verwende als Alternative das Recovery-Passwort oder kontaktiere einen anderen Administrator.</p>
		</details>
		
		<hr width="100%" />
		<input type="submit" value="Login" style="width:auto; padding: 0px 15px;">
		<?php
			if(isset($page['info'])){
				echo '<font color="#FF0000">' . $page['info'] . '</font><br />';
			}
		?>
	</form>
</html>