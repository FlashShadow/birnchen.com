<?php
header("X-UA-Compatible: Chrome=1");
header("X-Content-Type-Options: nosniff");
header("X-Frame-Options: deny");
header("X-XSS-Protection: 1; mode=block");
header("Content-Security-Policy: default-src 'self'; script-src 'none'; style-src 'self' 'unsafe-inline' cdn.jsdelivr.net; img-src 'self'; frame-src 'none'; report-uri /report.php ");
header("X-Content-Security-Policy: default-src 'self'; script-src 'none'; style-src 'self' 'unsafe-inline' cdn.jsdelivr.net; img-src 'self'; frame-src 'none'; report-uri /report.php ");
header("X-WebKit-CSP: default-src 'self'; script-src 'none'; style-src 'self' 'unsafe-inline' cdn.jsdelivr.net; img-src 'self'; frame-src 'none'; report-uri /report.php ");
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.
header("X-Powered-By: PHP/" . phpversion());

$struct = array(
	array(
		"file" => "login.page.php",
		"path" => "/admin/",
		"title" => "Admin Login",
		"uac" => -2,
		"icon" => "",
		"template" => false,
		"id" => 1
	),
	array(
		"file" => "login.form.php",
		"path" => "/admin/login.php",
		"title" => "Admin Form",
		"uac" => -2,
		"icon" => "",
		"template" => false,
		"robots" => "noindex",
		"id" => 2
	),
	array(
		"file" => "main.page.php",
		"path" => "/admin/main.php",
		"title" => "Admin-Panel",
		"uac" => 2,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 3
	),
	array(
		"file" => "accounts.page.php",
		"path" => "/admin/accounts.php",
		"title" => "Accounts",
		"uac" => 1,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 4
	),
	array(
		"file" => "accounts.edit.php",
		"path" => "/admin/accounts.edit.php",
		"title" => "Accounts Edit",
		"uac" => -2,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 4
	),
	array(
		"file" => "page.page.php",
		"path" => "/admin/page.php",
		"title" => "Site-Managment",
		"uac" => 2,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 5
	),
	array(
		"file" => "api.page.php",
		"path" => "/admin/api-key.php",
		"title" => "API-Keys",
		"uac" => 1,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 6
	),
	array(
		"file" => "api.edit.php",
		"path" => "/admin/api.edit.php",
		"title" => "API-Key Edit",
		"uac" => -2,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 6
	),
	array(
		"file" => "db.page.php",
		"path" => "/admin/db.php",
		"title" => "DB-Managment",
		"uac" => 2,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 7
	),
	array(
		"file" => "settings.page.php",
		"path" => "/admin/settings.php",
		"title" => "Settings",
		"uac" => 0,
		"icon" => "",
		"template" => 2,
		"robots" => "noindex",
		"id" => 8
	),
	array(
		"file" => "logout.page.php",
		"path" => "/admin/logout.php",
		"title" => "Logout",
		"uac" => 0,
		"icon" => "",
		"template" => false,
		"robots" => "noindex",
		"id" => 9
	)
);
