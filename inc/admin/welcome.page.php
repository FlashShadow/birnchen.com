<?php

if(!isset($row)){
	trigger_error("Missing var row",E_USER_ERROR);
}
?>
<html>
	<head>
		<!--<meta http-equiv="refresh" content="<?php echo $refresh_time; ?>;url=main.php">-->
		<style>
			body{
				background-color: #3B5998;
				z-index: 2;
				font-family: 'Times New Roman';
			}
			.content{
				position: absolute;
				left: 50%;
				top: 50%;
				width: 320px;
				margin-top: -100px;
				margin-left: -175px;
				background-color: #FFFFFF;
				padding-left: 15px;
				padding-top: 5px;
				padding-right: 15px;
				padding-bottom: 20px;
				z-index: 3;
				outline-color: #000000;
				outline-width: 1px;
				outline-style: solid;
			}
			.link{
				margin-top: 15px;
			}
		</style>
	</head>
	<body>
		<div class="content">
			<p><font color="#00FF00"><?php
				// Secial from login.page.php
					if(isset($special) && $special==1){
						echo "You are already signed in! <br><br>";
					}else{
						echo "Login successful";
					}
			 ?></font></p>
			<div class="info">
				<?php
					
					// Special from login.form.php
					if(isset($special) && $special==0){
						echo "You have an hour until the account is disabled! <br><br>";
					}
					
					// Special from row
					if($row['login_failed']>0){
						echo "Login failed for " . $row['login_failed'] . ' times!<br>';
						echo "<samp>Last fail on " . $row['last_update'] . "</samp><br>";
					}
				 ?>
			</div>
			<div class="link">
				<a href="main.php" >weiter</a>
			</div>
		</div>
	</body>
</html>