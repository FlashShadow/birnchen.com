<?php

// Prüfe Eingabe
if(!isset($_GET['action'])){
	header("HTTP/1.0 400 Bad Request");
	return;
}
include_once('inc/func/sql.func.php');
$mysqli = sql_connect($mysqli_user,$mysqli_pw,$mysqli_db,$mysqli_host);
include_once('inc/func/api_login.func.php');
// Prüfe Anmeldung
if(isset($_GET['user']) && $_GET['pw']){
	$login = api_login($_GET['user'],$_GET['pw']);
	if($login[0]===false){
		trigger_error("API Login Failed: " . var_export($login,true) ,E_USER_NOTICE);
	}
}else{
	$login = array(false,false);
}

switch($_GET['action']){
	case ('edit'):
		if($login[0]==false){
        	header("HTTP/1.0 401 Unauthorized"); 
			return;
		}
		include_once('inc/db/httpedit.modul.php');
		break;
	case ('view'):
		include_once('inc/db/view.modul.php');
		if(isset($output['login'])){ // Check if login is required
			if($login[0]==false){
				header("HTTP/1.0 400 Bad Request");
				unset($output);
				$output['INFO'] = "Please login to get help";
			}
		}
		include_once('inc/db/json.modul.php');
		break;
	case ('w2ui'):
		include_once('inc/db/view.modul.php');
		include_once('inc/db/w2ui.modul.php');
		include_once('inc/db/json.modul.php');
		break;
	case ('account'):
		if($login[1]!=false){
    		trigger_error("New Access Key: " . base64_encode($login[1]),E_USER_NOTICE);
			$output['KEY'] = base64_encode($login[1]);
    	}else{
    		$output['ERROR'] = "Unable to generate Access Key";
    	}
		include_once('inc/db/json.modul.php');
		break;
	default:
		header("HTTP/1.0 400 Bad Request");
		return;
}