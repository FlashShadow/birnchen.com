<?php

header("Content-type: application/json; charset=utf-8");
header("Expires: " . date("r",time()+60*60*24));

if(!isset($output)) trigger_error("API ARRAY_EMPTY",E_USER_ERROR);

$json = str_replace("ID","recid",str_replace(array("&nbsp;","<br>")," ",html_entity_decode(json_encode($output,JSON_PRETTY_PRINT),ENT_HTML5,"UTF-8")));
if($json===false){
	trigger_error("API JSON_ERROR: " . json_last_error_msg(),E_USER_ERROR);
}else{
	ob_start("ob_gzhandler");
	echo $json;
}