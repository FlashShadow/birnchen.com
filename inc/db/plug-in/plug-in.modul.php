<?php
function manage($url,$regex){
	$content = file_get_contents($url) or trigger_error("Can't open: " . $url,E_USER_WARNING);
	return parse($content,$regex);
}
function parse($code,$regex){
	preg_match_all($regex, $code, $matches);
	return $matches[0];		
}

function multihoster($hoster){
	if(!isset($hoster)){
		trigger_error("Missing the website",E_USER_ERROR);
	}
	$parser[0] = '/(?<=\'|\")([^\,\"]+)(?=\'|\")/'; // " or '
	$parser[1] = '/(?<=^|\n)(.*?)(?=\n|$)/'; // Parse lines
	$parser[2] = '/(?<=l\"\:\")([^\,]+)(?=\")/'; // Special JSON-Parser
	$parser[3] = '/([^\:\,\;\|]+)/'; // Special delemiter : or ; or , or |
	$parser[5] = '/(?<=\\\/\\\/|www\.)(?!www)([^\;]+)(?=\;|\")/'; // Special Parser
	$parser[6] = '/(?<=\<li\>)(.*?)(?=\<\/li\>)/'; // <li>-list
	$parser[7] = '/(?<=hostdomains\"\:\[\")(.*?)(?=\")/'; // Special JSON-Parser
	$parser[8] = '/(?<=\}\,\")(.*?)(?=\")/'; // Special JSON-Parser
	$parser[9] = '/(?<=\[\")([^\"\,]+)(?=\")/'; // Special JSON-Parser
	$parser[10] = '/(?<=\")(?!ok)([^\"\,]+)(?=\"\,|\"\])/'; // Special JSON-Parser
	$parser[11] = '/(?<=\")([^\"]+)(?=\"\:)/'; // Special JSON-Parser
	$parser[13] = '/(?<=\<host\>)(.*?)(?=\<\/host\>)/'; // Parse <host>-list
	
	//TODO: Check if you need a Fuzzy-String-Search
	switch($hoster){
		case 'alldebrid.com':
			$support = manage("http://www.alldebrid.com/api.php?action=get_host",$parser[0]);
			break;
		case 'cokluindir.com':
			$support = manage("http://cokluindir.com/saglayicilar.php/",$parser[0]);
			break;
		case 'debriditalia.com'	:
			$support = manage("http://debriditalia.com/api.php?hosts",$parser[0]);
			break;
		case 'downitfaster.com':
			$support = manage("http://downitfaster.com/api/supportedHosts.php",$parser[1]);
			break;
		case 'downmasters.com':
			$support = manage("http://downmasters.com/hostapi.php",$parser[2]);
			break;
		case 'easyfiles.pl':
		case 'rapids24.pl':
		case 'turbix.pl':
			$support = manage("http://easyfiles.pl/api2.php?cmd=get_hosts",$parser[3]);
			break;
		case 'esoubory.cz':
			$support = manage("http://www.esoubory.cz/api/list",$parser[5]);
			break;
		case 'ffdownloader.com':
			$support = manage("http://ffdownloader.com/api/v1/hosters?_format=json",$parser[6]);
			break;
		case 'filebit.pl':
			$support = manage("http://filebit.pl/api/index.php?a=getHostList",$parser[7]);
			break;
		case 'filenium.com':
			$support = manage("http://filenium.com/jddomains",$parser[0]);
			break;
		case 'free-way.me':
			$support = manage("https://www.free-way.me/ajax/jd.php?id=3",$parser[0]);
			break;
		case 'linksnappy.com':
			$support = manage("http://gen.linksnappy.com/lseAPI.php?act=FILEHOSTS&username=49249&password=ec22fd4a11cf989c5b90e3e2e8308b2e",$parser[8]);
			break;
		case 'mega-debrid.eu':
			$support = manage("http://mega-debrid.eu/api.php?action=getHosters",$parser[9]);
			break;
		case 'multi-debrid.com':
			$support = manage("http://multi-debrid.com/api.php?hosts",$parser[10]);
			break;
		case 'multidown.co.il':
			$support = manage("http://multidown.co.il/api.php?hosts=1",$parser[0]);
			break;
		case 'multihosters.com':
			$support = manage("http://www.multihosters.com/jDownloader.ashx?cmd=gethosters",$parser[3]);
			break;
		case 'nopremium.pl':
			$support = manage("http://www.nopremium.pl/clipboard.php",$parser[1]);
			break;
		case 'twojlimit.pl':
			$support = manage("https://www.twojlimit.pl/clipboard.php",$parser[1]);
			break;
		case 'olget.com':
			$support = manage("http://olget.com/api/filehosts",$parser[11]);
			break;
		case 'over-load.me':
			$support = manage("https://api.over-load.me/hoster.php?auth=0001-9ada329cf5894f360d13ac3b91588a1b530a76ee48814-530a76ee-4c01-3ab012ec",$parser[0]);
			break;
		case 'premiumize.me':
			$support = json_decode(file_get_contents("https://api.premiumize.me/pm-api/v1.php?method=hosterlist&params[login]=pspzockerscene&&params[pass]=C2zafs8IIu"));
			$support = $support["result"]["tldlist"];
			break;
		case 'putdrive.com':
			$support = strtolower(manage("http://putdrive.com/jdownloader.ashx?cmd=gethosters",$parser[3]));
			break;
		case 'real-debrid.com':
			$support = manage("https://real-debrid.com/api/hosters.php",$parser[0]);
			break;
		case 'premium.rpnet.biz':
			$support = manage("https://premium.rpnet.biz/hostlist.php",$parser[3]);
			break;
		case 'sharedir.com':
			$support = manage("http://sharedir.com/sdapi.php?get_dl_hosts",$parser[3]);
			break;
		case 'simply-debrid.com':
			$support = manage("http://simply-debrid.com/api.php?list=1",$parser[3]);
			break;
		case 'streammania.com':
		case 'brapid.sk':
		case 'megafastlink.eu':
			$support = manage("http://www.streammania.com/api/get_filehosters.php",$parser[3]);
			break;
		case 'unrestrict.li':
			$support = manage("http://unrestrict.li/api/jdownloader/hosts.php",$parser[13]);
			break;
/*			
	http://debrid-link.fr/api/?act=1 - offline
	https://www.download.me/dlapi/hosters - didn't answer
	https://www.multishare.cz/api/?sub=supported-hosters - unexcepected result
	https://pivit.load/api/hosts.php?user=xx&pw=xx - offline
	http://premium.to/hosts.php - empty

premiumdebrid.com
Eventuell momentan down - hat keine API, erstmal ignorieren
http://premiumdebrid.com

Kompliziertere Sachen mit Login-Vorgang und/oder tokens:

superload.cz
POST
http://api.superload.cz/a-p-i/login
username=dev%40jdownloader.org&password=abf23ed44f7796d2a7784d7589670c5c
Hier token holen (name "token")
POST
http://api.superload.cz/a-p-i/get-supported-hosts
token=AUTH_TOKEN
http://api.zevera.com/jDownloader.ashx?cmd=gethosters

rehost.to
Token holen (Wert von "long_ses"):
https://rehost.to/api.php?cmd=login&user=19140&pass=4539huzg5rhjeh
https://rehost.to/api.php?cmd=get_supported_och_dl&long_ses=AUTH_TOKEN

mydownloader.net
Erst token holen:
http://api.mydownloader.net/api.php?task=auth&login=sytyi@spikio.com&password=thrtztjbvehz
dann dieser Aufruf:
http://api.mydownloader.net/api.php?task=supported_list&auth=AUTH_TOKEN
*/
	}	
}

