<?php
function listtable($mysqli){
	$output['ERROR'] = "You need to specify in which table you want to work!"; 
	// Zeige mögliche Tabellen, wenn angemeldet
	$output['QUERY'][] = "SELECT * FROM lists";
	$result = mysqli_query($mysqli,end($output['QUERY'])) or trigger_error(mysqli_error(),E_USER_WARNING);
	$i = 0;
	while($row=mysqli_fetch_assoc($result)){
		$output['records'][$i]['name'] = sql($row['name']);			
		$output['records'][$i]['table'] = sql($row['table']);
		$i ++;
	}
	$output['total'] = $i;
	$output['login'] = 'require';
	return $output;
}


// Frage Tabellen ab
$output['QUERY'][] = "SELECT * FROM lists";
$result = mysqli_query($mysqli,end($output['QUERY'])) or trigger_error(mysqli_error(),E_USER_WARNING);
$i = 0;
$table = array();
while($row=mysqli_fetch_assoc($result)){
	$table[$i]['name'] = $row['name'];			
	$table[$i]['table'] = $row['table'];
	$i ++;
}
// Vergleiche, ob Tabelle vorhanden
$found = false;
if(isset($_GET['table'])){
	foreach($table as $value){
		if(strtolower($_GET['table'])==strtolower($value['table'])){
			$found = true;	
			$output['table'] = sql($value['table']);
			$output['name'] = sql($value['name']);
			break;
		}
	}
}
if($found == false){
	header("HTTP/1.0 400 Bad Request");
	$output = listtable($mysqli);
	return;
}
$table = $value['table']; // Wichtig für SQL-Query
// Gebe Tabelle aus
$output['QUERY'][] = "SELECT * FROM " . $table; // Nicht von User-Eingabe, sondern DB Abfrage!!
$result = mysqli_query($mysqli,end($output['QUERY'])) or trigger_error(mysqli_error(),E_USER_WARNING);
$i = 0;
while($row=mysqli_fetch_assoc($result)){
	foreach($row as $key => $value){
		if($key=="ID"){
			$output['records'][$i][$key] = (int) sql($value);
		}elseif($key=="Kommentar" and !empty($value)){
			$output['records'][$i][$key] = sql(trim(str_replace(".  ","",str_replace(".",". ",preg_replace("#(\r|\n|\r\n)#", '', $value)))));
			$output['records'][$i]['style'] = "color: #000080;font-style: italic;";
		}else{
			$output['records'][$i][$key] = sql($value);	
		}
	}
	$i ++;
}
$output['total'] = $i;
			
// Security Reason
if($login[0]==false && isset($output['QUERY'])){
	unset($output['QUERY']);
}