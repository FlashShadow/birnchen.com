<html>
    <head>
        <!-- Kopie von style.css vom 23.03.12 9:20 -->
        <style>
                *{
                    margin: 0px;
                    padding: 0px;
                }
                body{
                    background-color: #000000;
                    color: #EAEAEA;
                }
                p{
                    padding: 10px;
                }
                a:hover{
                    color: #00FFFF;
                    background-color: #000000;
                }
                a{
                    text-decoration: none;
                    color: #FFFFFF;
                }
                img{
                    padding: 10px;
                    border: 0px;
                }
                
                
                
                
                
                
                
                /* News */
                #newslink{
                    color: #EAEAEA;
                    text-decoration: none;
                }
                #news{
                    font-size: 12px;
                }
                
                /* Login php und Regestrieren.php */
                #email{
                    width: 100px;
                    background-color: #EAEAEA;
                }
                #passwort, #passwortw{
                    width: 100px;
                    background-color: #EAEAEA;
                }
                
                /* Navigation */
                #navitable{
                    width: 100%;
                
                }
                #navitr{
                    height: 30px;
                    background-color: #000000;
                }
                .navitd{
                    cursor: pointer;
                }
                
                
                /* Foot */
                #foodtr{
                
                }
                #foodtd{
                    width: 196px;
                    color: #636363;
                }
                
                
                
                
                
                /* Divs */
                
                
                /* Alles */
                #all{
                 width: 980px;
                 margin-top: 30px;
                 margin-bottom: 30px;
                }
                /* Banner*/
                #banner{
                    width: 980px;
                    height: 250px;
                }
                /* Menu */
                #menu{
                    width: 980px;
                  /*  margin-bottom: 30px;    */
                }
                /* Inhalt/navi */
                #navi{
                    float: left;
                    width: 150px;
                    text-align: left;
                    list-style-type: disc;
                }
                /* Inhalt/content */
                #content{
                    float: left;
                	width: 620px;
                	text-align: left;
                	padding: 5px;
                }
                /* Inhalt/news */
                #news{
                    float: right;
                    width: 200px;
                    text-align: left;
                }
                /* Foot */
                #foot{
                    padding: 40px;
                    clear: both;
                    width: 980px;
                    background-color: #010101;
                    float: right;
                    text-align: right;
                }
        </style>     
    </head>
<body>
    <center>
        <div id="all">
                <div id="Banner">
                    <div id="logo">
                        <br /><br />
                        <?php 
                                /*******************************
                                 *   (c) by Berthold Niemann   *
                                 *******************************/
                        
                        
                            $error = $_GET['error'];
                            
                            echo "<h1 style=\"color: #FF0000;\">";
                            echo "Error-Code " . $error . ": ";
                            
                            if($error=="000"){
                                echo "Network Connection Error";
                            }elseif($error=="100"){
                                echo "Continue";    
                            }elseif($error=="101"){
                                echo "Switching Protocols";
                            }elseif($error=="102"){
                                echo "Processing";
                            }elseif($error=="103"){
                                echo "Checkpoint";
                            }elseif($error=="118"){
                                echo "Connection timed out";    
                            }elseif($error=="122"){
                                echo "Request-URI too long";
                            }elseif($error=="200"){
                                echo "OK";
                            }elseif($error=="201"){
                                echo "Created";
                            }elseif($error=="202"){
                                echo "Accepted";
                            }elseif($error=="203"){
                                echo "Non-Authoritative Information";
                            }elseif($error=="204"){
                                echo "No Content";
                            }elseif($error=="205"){
                                echo "Reset Content";
                            }elseif($error=="206"){
                                echo "Partial Content";
                            }elseif($error=="207"){
                                echo "Multi-Status";
                            }elseif($error=="208"){
                                echo "Already Reported";
                            }elseif($error=="226"){
                                echo "IM Used";
                            }elseif($error=="300"){
                                echo "Multiple Choices";
                            }elseif($error=="301"){
                                echo "Moved Permanently";
                            }elseif($error=="302"){
                                echo "Found";
                            }elseif($error=="303"){
                                echo "See Other";
                            }elseif($error=="304"){
                                echo "Not Modified";
                            }elseif($error=="305"){
                                echo "Use Proxy";
                            }elseif($error=="306"){
                                echo "Switch Proxy";
                            }elseif($error=="307"){
                                echo "Temporary Redirect";
                            }elseif($error=="308"){
                                echo "Resume Incomplete";
                            }elseif($error=="400"){
                                echo "Bad Request";
                            }elseif($error=="401"){
                                echo "Unauthorized";
                            }elseif($error=="402"){
                                echo "Payment Required";
                            }elseif($error=="403"){
                                echo "Forbidden";
                            }elseif($error=="404"){
                                echo "Not Found";
                            }elseif($error=="405"){
                                echo "Method Not Allowed";
                            }elseif($error=="406"){
                                echo "Not Acceptable";
                            }elseif($error=="407"){
                                echo "Proxy Authentication Required";
                            }elseif($error=="408"){
                                echo "Request Timeout";
                            }elseif($error=="409"){
                                echo "Conflict";
                            }elseif($error=="410"){
                                echo "Gone";
                            }elseif($error=="411"){
                                echo "Length Required";
                            }elseif($error=="412"){
                                echo "Precondition Failed";
                            }elseif($error=="413"){
                                echo "Request Entity Too Large";
                            }elseif($error=="414"){
                                echo "Request-URI Too Long";
                            }elseif($error=="415"){
                                echo "Unsupported Media Type";
                            }elseif($error=="416"){
                                echo "Request Range Not Satisfiable";
                            }elseif($error=="417"){
                                echo "Expectation Failed";
/*April Scherz Status Code*/}elseif($error=="418"){
                                echo "I'm a teapot";
                            }elseif($error=="420"){
                                echo "Enhance Your Calm";
                            }elseif($error=="421"){
                                echo "There are too many connections from your internet adress";
                            }elseif($error=="422"){
                                echo "Unprocessable Entity";
                            }elseif($error=="423"){
                                echo "Locked";
                            }elseif($error=="424"){
                                echo "Failed Dependency";
                            }elseif($error=="425"){
                                echo "Unordered Collection";
                            }elseif($error=="426"){
                                echo "Upgrade Required";
                            }elseif($error=="428"){
                                echo "Precondition";
                            }elseif($error=="429"){
                                echo "Too Many Requests";
                            }elseif($error=="431"){
                                echo "Request Header Fields Too Large";
                            }elseif($error=="444"){
                                echo "No Response";
                            }elseif($error=="449"){
                                echo "Retry With";
                            }elseif($error=="450"){
                                echo "Blocked by Widnows Parental Controls";
                            }elseif($error=="499"){
                                echo "Client Closed Request";
                            }elseif($error=="500"){
                                echo "Internal Server Error";
                            }elseif($error=="501"){
                                echo "Not Implemented";
                            }elseif($error=="502"){
                                echo "Bad Gateway";
                            }elseif($error=="503"){
                                echo "Service Unviable";
                            }elseif($error=="504"){
                                echo "Gateway Time-out";
                            }elseif($error=="505"){
                                echo "HTTP Version not supported";
                            }elseif($error=="506"){
                                echo "Variant Also Negotiates";
                            }elseif($error=="507"){
                                echo "Insufficient Storage";
                            }elseif($error=="508"){
                                echo "Loop Detected";
                            }elseif($error=="509"){
                                echo "Bandwidth Limit Exeeded";
                            }elseif($error=="510"){
                                echo "Not Extended";
                            }elseif($error=="511"){
                                echo "Network Authentication Required";
                            }elseif($error=="512"){
                                echo "Not Supported";
                            }elseif($error=="598"){
                                echo "Network read Timeout Error";
                            }elseif($error=="599"){
                                echo "Network connect Timeout Error";
                            }elseif($error=="990"){
                                echo "Blocked by robots.txt file";
                            }elseif($error=="999"){
                                echo "Unknow or Indeterminate";
                            }else{
                                echo "</h1><h1>! ! !</h1>PHP-Fehler bei der Ausgabe, keine passende Nummer gefunden<h1>! ! !";
                            }
                            
                            echo "</h1>";
                            
                            
                            // mehr Error codes: http://www.indigorose.com/webhelp/vp/Program_Reference/Actions/Error_Codes.htm#System_Related
                         ?>
                        <br /><br /><br />
                        <input type="button" onclick="index.php" value="Zur&uuml;ck" />
                    </div>
                </div>
        </div>
    </center>
</body>
</html>