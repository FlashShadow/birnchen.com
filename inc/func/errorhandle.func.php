<?php
function myErrorHandler($fehlercode, $fehlertext, $fehlerdatei, $fehlerzeile,$fehlercontext)
{
	$message = date("r") . " | " . $fehlercode . " | " . $fehlertext . " | " . $fehlerzeile . " | " . $fehlerdatei . " | " . $_SERVER['REMOTE_ADDR']  . " | " . $_SERVER['REQUEST_URI'] . " | " . $_SERVER['HTTP_REFERER'];
    switch ($fehlercode) {
    	case E_USER_ERROR:
			header("HTTP/1.0 500 Internal Server Error");
			error_log($message . "\r\n" . var_export($fehlercontext,true),3,"inc/logs/ERROR_" . date("U") . ".log");
			if(stripos($fehlertext,"Emergency")!==false){
				// Behandle Emergency-Fehler
				include_once('inc/func/var_save.func.php');
				save_var(array_merge(array($message),$fehlercontext),"offline.php");
			}
			include_once('inc/template-error.php');
	    case E_USER_WARNING:
	    case E_USER_NOTICE:
	    default:
			error_log($message . "\r\n",3,"inc/logs/error.log");
    }

    /* Damit die PHP-interne Fehlerbehandlung nicht ausgeführt wird */
    return true;
} 
set_error_handler("myErrorHandler");