<?php

function check_login(){
	global $query,$user,$rights;
	// var user for welcome.page.php
	
	if($user = false){
		return false;
	}
	
	if(!isset($_COOKIE['login']) || !isset($_COOKIE['user']) || !isset($_COOKIE['id'])){
		return false;
	}
	include_once('inc/func/sql.func.php');
	$mysqli = sql_connect();
	
	$query[] = "SELECT * FROM users WHERE id='" . mysqli_real_escape_string($mysqli,$_COOKIE['id']) . "';";
	$result = mysqli_query($mysqli,end($query)) or trigger_error(mysqli_error($mysqli),E_USER_WARNING);
	
	if($result!==false){
		$row = mysqli_fetch_assoc($result);
		if(user($row['user'])==$_COOKIE['user'] && $_COOKIE['login']==$row['login_key'] && $row['login_key']!=false){
			// Kill Session of deactivated accounts or accounts or with wrong ip
			if($_SERVER["REMOTE_ADDR"]==$row['last_ip'] && $row['active']==1){
				resfresh_login();	
				$rights = $row['uac'];
				$user = $row;
				return true;
			}else{
				if($row['active']==0){
					trigger_error("Session killed, because of deactivated acc: " . $user,E_USER_NOTICE);
				}else{
					trigger_error("Security Logoff for User " . $row['user'] . " because old IP: " . $row['last_ip'] . " new IP:" . $_SERVER["REMOTE_ADDR"],E_USER_ERROR);
				}
				$rights = 0;
				$user = false;
				log_off($row);
				return false;
			}
		}else{
			$user = false; // For next check
			$rights = 0;
		}
	}else{
		$user = false; // For next check
		$rights = 0;
	}
}

function resfresh_login(){
	global $cookietime;
	setcookie("user",$_COOKIE['user'],$cookietime);
	setcookie("login",$_COOKIE['login'],$cookietime);
	setcookie("id",$_COOKIE['id'],$cookietime);
}


function set_login($row){
	global $query,$cookietime;
	$user = user($row['user']);
	$pw = pw($row['password']);
	
	include_once('inc/func/sql.func.php');
	$mysqli = sql_connect();
	
	$query[] = "UPDATE users SET login_key='" . mysqli_real_escape_string($mysqli,$pw) . "', last_ip='" . mysqli_real_escape_string($mysqli,$_SERVER['REMOTE_ADDR']) . "', login_failed=0 WHERE id=" . $row['id'];
	$result = mysqli_query($mysqli,end($query)) or trigger_error(mysqli_error($mysqli),E_USER_WARNING);
	if($result===false){
		trigger_error("EMERGENCY: Can't write new login_key in users-table",E_USER_ERROR);	
		return false;	
	}
	setcookie("user",$user,$cookietime);
	setcookie("login",$pw,$cookietime);
	setcookie("id",$row['id'],$cookietime);
}

function log_off($row){
	global $query;
	setcookie("user","",-60*60*24*3);
	setcookie("login","",-60*60*24*3);
	setcookie("id","",time()-60*60*24*3);
	
	include_once('inc/func/sql.func.php');
	$mysqli = sql_connect();
	
	$query[] = "UPDATE users SET login_key=false WHERE id=" . $row['id'];
	$result = mysqli_query($mysqli,end($query)) or trigger_error(mysqli_error($mysqli),E_USER_WARNING);
	if($result===false){
		trigger_error("EMERGENCY: Can't logoff User-Acc",E_USER_ERROR);	
	}
}

function search_user($user){
	global $query;
	
	include_once('inc/func/sql.func.php');
	$mysqli = sql_connect();
	
	$query[] = "SELECT * FROM users WHERE user='" . mysqli_real_escape_string($mysqli,$user) . "';";
	$result = mysqli_query($mysqli,end($query)) or trigger_error(mysqli_error($mysqli),E_USER_WARNING);
	
	if($result!==false){
		$row = mysqli_fetch_assoc($result);
		if(isset($row['id'])){
			return $row;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function user($user){
	$hash = md5($user . sha1("@birnchen.com") . date("YwdF",time()-60*2));
	return $hash;
}
function pw($pw){
	$pw = sha1(md5($pw) . base64_encode("birnchen.com"));	
	return $pw;
}

function acc_active($row,$active){
	global $query;
	
	if(!is_bool($active)){
		trigger_error("$active isn't a boolean",E_USER_ERROR);
	}
	
	include_once('inc/func/sql.func.php');
	$mysqli = sql_connect();
	
	$query[] = "UPDATE users SET active=" . $active . " WHERE id=" . $row['id'];
	$result = mysqli_query($mysqli,end($query)) or trigger_error(mysqli_error($mysqli),E_USER_WARNING);
	if($result===false){
		trigger_error("EMERGENCY: Change active state",E_USER_ERROR);	
	}
	
}
