<?php

function menu(){
	global $struct,$page,$disabled;
	if(!isset($struct)){
		trigger_error("No Page Table available",E_USER_ERROR);
	}
	$request_uri = urldecode($_SERVER['REQUEST_URI']);
	
	$page['menu'] = '';
	for($i=0;$i<count($struct);$i++){
		$data = $struct[$i];
		
		// Check rights
		include_once('inc/func/user_rights.func.php');
		if(isset($data['uac']) && check_rights($data['uac'])==false){
			if(stripos($request_uri,$data['path'])!==false){
				// No Permissions, but this is the requested page
				if(!isset($page['active'])){
					$disabled = $data['id'];
				}else{
					// Wildcard will be used
					// Protect against access:
					continue;
				}
			}
			continue;
		}else if($data['uac']==-2){
			if(stripos($request_uri,$data['path'])!==false){
				$page['active'] = $i;
				$page['title'] = $data['title'];
			}
			continue;
		}
		
		$page['menu'] .= '<li class="';
		if(stripos($request_uri,$data['path'])!==false){
			$page['title'] = $data['title'];
			$page['menu'] .= 'active current';
			$page['active'] = $i;
		}
		$page['menu'] .= '">';
		if(isset($data['icon'])){
			$page['menu'] .= '<a class="' . $data['icon'] . '" ';
		}else{
			$page['menu'] .= '<a ';
		}
		$page['menu'] .= 'href="' . $data['path'] . '" title="' . $data['title'] . '"><span>' . $data['title'] . '</span></a></li>';
	}
}
