<?php
function sql_connect(){
	global $mysqli,$mysqli_user,$mysqli_pw,$mysqli_db,$mysqli_host;	
	if(isset($mysqli)){
		return $mysqli;
	}
	$mysqli = mysqli_connect($mysqli_host,$mysqli_user,$mysqli_pw,$mysqli_db) or die(header("HTTP/1.0 500 Internal Server Error"));
	mysqli_query($mysqli,"SET NAMES 'utf8'");	
	return $mysqli;
}


// SQL Help Tool for Unicode
function sql($data){
	return htmlentities($data,ENT_HTML5,"UTF-8");
}