<?php

if(file_exists("inc/install")){
	echo "already installed - please delete the file 'install' to re-import the database";	
	exit;
}

// Name of the file
$filename = 'inc/birnchen_new.sql';

// Connect to MySQL server
include_once("inc/func/sql.func.php");
$mysqli = sql_connect();
// Select database

// Temporary variable, used to store current query
$templine = '';
// Read in entire file
$lines = file($filename);
// Loop through each line
foreach ($lines as $line){
	// Skip it if it's a comment
	if (substr($line, 0, 2) == '--' || $line == '')
	    continue;
	
	// Add this line to the current segment
	$templine .= $line;
	// If it has a semicolon at the end, it's the end of the query
	if (substr(trim($line), -1, 1) == ';'){
	    // Perform the query
	    mysqli_query($mysqli,$templine) ? $log[] = array("SUCCESS" => true,"QUERY" => $templine) : $log[] = array("SUCCESS" => false,"QUERY" => $templine,"ERROR" => mysqli_error());
		
	    // Reset temp variable to empty
	    $templine = '';
	}
}
echo "Tables imported successfully";

include_once("inc/func/var_save.func.php");

save_var($log,"inc/install");

if(in_array(array("SUCCESS" => false),$log)){
	echo "There are some errors, please check inc/install";
}
