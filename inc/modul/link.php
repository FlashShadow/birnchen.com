<?php	
	if(!isset($_GET['page'])){
		header("HTTP/1.0 400 Bad Request");	
		$connector->getDebugDispatcher()->dispatchDebug('Bad Request');
		if(isset($_SERVER["HTTP_REFERER"])){
			header('Location: ' . $_SERVER["HTTP_REFERER"]);
		}else{
			header('Location: /');
		}
		die();
	}
	
	include_once('inc/page/struct.php');	
	
	foreach($struct as $struct_arrays){
		if(!isset($struct_arrays['id'])){
			trigger_error($struct_arrays['file'] . " has no id",E_USER_NOTICE);
		}else{
			if($struct_arrays['id']==$_GET['page']){
				$array = $struct_arrays;	
				break;
			}
		}
	}
	// Nothing found
	if(!isset($array)){
		header("HTTP/1.0 404 Not Found");
		if(isset($_SERVER["HTTP_REFERER"])){
			header('Location: ' . $_SERVER["HTTP_REFERER"]);
		}else{
			header('Location: /');
		}
		trigger_error("File not found: " . $lang . ' & id=' . $_GET['page'],E_USER_NOTICE);
		die();	
	}
	
	header("HTTP/1.0 303 See Other");
	header("Location: /" . $lang . "/". $array['path']);