<?php
$struct = array(
	array(
		"file" => "main.de.html",
		"path" => "/de/index.php",
		"title" => "Startseite",
		"icon" => "flaticon-dwelling1",
		"id" => 1
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/News.php",
		"title" => "News",
		"icon" => "flaticon-news3",
		"id" => 2
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/File-Hoster-Liste.php",
		"title" => "File-Hoster Liste",
		"icon" => "flaticon-box2",
		"id" => 3
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Pic-Hoster.php",
		"title" => "Pic-Hoster",
		"icon" => "flaticon-landscape",
		"id" => 4
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Multi-Hoster.php",
		"title" => "Multi-Hoster",
		"icon" => "flaticon-folder112",
		"id" => 5
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Multi-File Upload.php",
		"title" => "Multi-File Upload",
		"icon" => "flaticon-upload4",
		"id" => 6
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Trash-Mail.php",
		"title" => "Trash-Mail",
		"icon" => "flaticon-email6",
		"id" => 7
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Protect your Links.php",
		"title" => "Protect your Links",
		"icon" => "flaticon-lock2",
		"id" => 8
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Kurze URL.php",
		"title" => "Kurze URL",
		"icon" => "flaticon-link3",
		"id" => 9
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Online-Tools.php",
		"title" => "Online-Tools",
		"icon" => "flaticon-technical",
		"id" => 10
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Virenscanner.php",
		"title" => "Virenscanner",
		"icon" => "flaticon-antivirus",
		"id" => 11
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Freespace Webhosting.php",
		"title" => "Freespace Webhosting",
		"icon" => "flaticon-data26",
		"id" => 12
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Captcha Lösungen.php",
		"title" => "Captcha Lösungen",
		"icon" => "flaticon-pencil41",
		"id" => 13
	),
	array(
		"file" => "main.de.html",
		"path" => "/de/Kontakt.php",
		"title" => "Kontakt",
		"icon" => "flaticon-reply3",
		"id" => 14
	)
);