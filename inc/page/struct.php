<?php

//Language-Header
if(strpos($_SERVER['REQUEST_URI'],"/de/")===false && strpos($_SERVER['REQUEST_URI'],"/en/")===false){
	if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'],"de")!==false){
		header("HTTP/1.0 303 See Other");
		header('Location: /de' . $_SERVER['REQUEST_URI']);
		die();
	}else{
		header("HTTP/1.0 303 See Other");
		header('Location: /en' . $_SERVER['REQUEST_URI']);
		die();
	}	
}


if(strpos($_SERVER['REQUEST_URI'],"de")!==false){
	$lang = 'de'; // For Link.php
	include_once('/inc/page/de/struct.de.php');	
}else{
	$lang = 'en';
	include_once('/inc/page/en/struct.en.php');	
}

include_once('inc/page/xx/struct.xx.php');
$struct = array_merge($struct,$struct_xx);
