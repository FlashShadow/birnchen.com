<?php

// Chrome Extension Connection or PHP Error-Handler
require_once(__DIR__ . '/PhpConsole/__autoload.php');

$handler = PhpConsole\Handler::getInstance();
$handler->start(); // start handling PHP errors & exceptions
$handler->getConnector()->setSourcesBasePath($_SERVER['DOCUMENT_ROOT']); // so files paths on client will be shorter (optional)

$connector = PhpConsole\Connector::getInstance();

if(($_SERVER["REMOTE_ADDR"]=="localhost" || $_SERVER['REMOTE_ADDR']=="127.0.0.1" || $_SERVER['REMOTE_ADDR']=='::1') &&
	!((isset($_COOKIE['public']) and $_COOKIE['public']==true) or (isset($_GET['public']) and $_GET['public']==true))){
	$connector->getDebugDispatcher()->dispatchDebug('Debug Console aktive @ ' . $_SERVER['REQUEST_URI']);
}else{
	$connector->setPassword($phpconolepw, true);
	include_once('inc/func/errorhandle.func.php');	
}

//Sicherheitsprüfung
if(isset($_SERVER["HTTP_HOST"]) && $_SERVER["SERVER_NAME"]!=$_SERVER["HTTP_HOST"]){
	trigger_error("An other Host is requested",E_USER_ERROR);
	die();
}
if(file_exists('offline.php') && strpos($_SERVER['REQUEST_URI'],"/admin/")===false){
	header("HTTP/1.0 503 Service Unavailable");
	$connector->getDebugDispatcher()->dispatchDebug("An Emergency-Error occurred - Please check the logs and delete the file 'offline.php'. While this file exists, this website stay offline");
	die($_SERVER["HTTP_HOST"] . " will be back soon as possible");
}