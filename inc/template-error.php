<?php
/*
 * The code below gives an error message and can hide incomplete html-code after displaying. 
 *
 * Should JavaScript be enabled the previous HTML code can be removed from the DOM tree and displayed.
 * 
 * MIT License by Berthold Niemann
 * 
 * 
 * */

if(!headers_sent()){
	header("Content-Security-Policy: default-src 'self'; script-src 'unsafe-inline'; style-src 'unsafe-inline'; img-src 'none'; frame-src 'none';");
	header("X-Content-Security-Policy: default-src 'self'; script-src 'unsafe-inline'; style-src 'unsafe-inline'; img-src 'none'; frame-src 'none';");
	header("X-WebKit-CSP: default-src 'self'; script-src 'unsafe-inline'; style-src 'unsafe-inline'; img-src 'none'; frame-src 'none';");
}

echo '
<script>
var html = document.documentElement.innerHTML;
</script>
<div style="
position:fixed !important;
left:0 !important; 
top:0 !important;
color: #FF0000 !important;
background-color: #FFFFFF !important;
width: 80% !important;
height: 80% !important;
padding-top: 20% !important;
padding-left: 20% !important;
z-index:100 !important;
overflow:visible !important;
font-family: Arial !important;
font-size: 20px !important;
">
An error has occurred. We will fix it as soon as possible.
</div>
<script type="text/javascript">
function htmlEntities(str) {
    return String(str).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
}
document.documentElement.innerHTML 
= "<html>"
+ "<body style=\"overflow-x: hidden !important;\">"
+ "<div style=\""
+ "position: static !important;"
+ "left:0 !important; "
+ "top:0 !important;"
+ "color: #FF0000 !important;"
+ "background-color: #FFFFFF !important;"
+ "width: 80% !important;"
+ "height: 80% !important;"
+ "padding-top: 20% !important;"
+ "padding-left: 20% !important;"
+ "z-index:100 !important;"
+ "overflow: visible !important;"
+ "font-family: Arial !important;"
+ "font-size: 20px !important;"
+ "\">"
+ "An error has occurred. We will fix it as soon as possible.<br><br><br>"
+ "Last output: <br>"
/*
+ "<iframe src=\"javascript:\'content\'\" style=\"height: 200px; width: 80%; \"></iframe>"
+ "<scri" + "pt>"
+ "var ifrm = document.getElementsByTagName(\'iframe\')[0];"
+ "ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;"
+ "ifrm.document.open();"
+ "ifrm.document.write(unescape(\'" + escape(html) + "\'));"
+ "</scr" + "ipt>"
*/
+ "<br><pre style=\"white-space: pre-wrap; width:80%;\">"
+ htmlEntities(html)
+ "</pre><\/div>";
+ "</body>"
+ "</html>"
</script>
';
