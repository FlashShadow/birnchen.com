<?php
include_once('inc/config.php');
include_once('inc/startup.php');


// Splitte in API und Webseite
if(isset($_SERVER['REDIRECT_URL']) && strpos($_SERVER['REDIRECT_URL'],"Error.php")!==false){
	include_once('inc/error.php');
}elseif(strpos($_SERVER['REQUEST_URI'],"api.php")!==false){
	include_once('inc/db/api.modul.php');
}elseif(strpos($_SERVER['REQUEST_URI'],"link.php")!==false){
	include_once('inc/modul/link.php');
}elseif(strpos($_SERVER['REQUEST_URI'],"install.php")!==false){
	include_once('inc/modul/install.php');
}elseif(strpos($_SERVER['REQUEST_URI'],"report.php")!==false){
	include_once('inc/modul/report.php');
}else{
	// Check URL (not requried if you use htaccess redirects)
	if(strpos($_SERVER['REQUEST_URI'],".php")===false){
		header("HTTP/1.0 303 See Other");
		if(strpos($_SERVER['REQUEST_URI'],"/")==count($_SERVER['REQUEST_URI'])-1){
			header('Location: ' . $_SERVER['REQUEST_URI'] . "index.php");
		}else{
			header('Location: ' . $_SERVER['REQUEST_URI'] . "/index.php");
		}
		die();
	}
	
	if(strpos($_SERVER['REQUEST_URI'],"/admin/")!==false){ // strpos for perfomance reason
		include_once('inc/admin/struct.php');
		// TODO: Delete this comment if dev-close
		echo $masteruser . "<br>";
		echo $masterpw;
	}else{
		include_once('inc/page/struct.php');
	}
	
	include_once('inc/func/menu.func.php');
	menu();
	
	// If not set
	if(!isset($page['active'])){
		// Check: Why?
		if(isset($disabled)){
			// We have no permissions
			header("HTTP/1.0 403 Forbidden");
			$_GET['error'] = 403;
			include_once('inc/error.php');	
			exit();
		}
		// We don't know why.
		trigger_error("NOTICE: No existing active page found",E_USER_NOTICE);
		// We have no permissions
		header("HTTP/1.0 404 Not Found");
		$_GET['error'] = 404;
		include_once('inc/error.php');	
		exit();
	}
	
	
	if(!isset($lang)){
		$inc = "inc/admin/" . $struct[$page['active']]['file'];
	}else{
		$inc = "inc/page/" . $lang . "/" . $struct[$page['active']]['file'];
	}
	if(!file_exists($inc)){
		trigger_error("Error loading ressource: " . $inc,E_USER_WARNING);
	}else{
		include_once($inc);
	}
	
	if(isset($struct[$page['active']]['robots']) && stripos($struct[$page['active']]['robots'],"noindex")!==false){
		header("X-Robots-Tag: none, noarchive, noimageindex, nofollow, noindex, nosnippet, notranslate");
	}
	
	if(!(isset($page['content']) && !empty($page['content']))){
		$page['content'] = "No content at this time: " . time();
	}
	
	// for lang.php
	if(!isset($struct[$page['active']]['id']) or empty($struct[$page['active']]['id'])){
		$page['id'] = 'ERROR';
		trigger_error("No id found for \$struct[" . $page['active'] . "]");
	}else{
		$page['id'] = $struct[$page['active']]['id'];
	}
	
	if(!isset($struct[$page['active']]['template']) || (isset($struct[$page['active']]['template']) && ($struct[$page['active']]['template']===true || $struct[$page['active']]['template']===1))){
		include_once('inc/template.php');	
	}else{
		if($struct[$page['active']]['template']!=false){
			$tmp = 'inc/template' . $struct[$page['active']]['template'] . '.php';
			if(file_exists($tmp)){
				include_once('inc/template' . $struct[$page['active']]['template'] . '.php');
			}else{
				trigger_error("Tamplate not found!",E_USER_ERROR);
			}
		}
	}
}/*else{
	include_once($_SERVER['REQUEST_URI']);
}*/

/*
 *  
TODO: Wartungssystem mit Auto-Backup
 * 
 Wenn struct-elemente die selbe id habe, sind sie miteinander gelinkt und werden über die Flaggen ansprechbar.
 
 */
